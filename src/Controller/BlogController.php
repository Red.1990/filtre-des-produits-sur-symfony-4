<?php

namespace App\Controller;


use App\Entity\Product;
use App\Data\SearchData;
use App\Form\SearchForm;
use App\Repository\ProductRepository;
use PhpParser\Node\Expr\New_;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
   
    /**
     * @Route("/", name="home_index")
     *
     * @return void
     */
    public function home(){
        return $this->render('home/index.html.twig');
    }


    /**
     * @Route("/product", name="home_product")
     *
     * @return Response
     */
    public function seance(ProductRepository $repository, Request $request)

    {
        // $reposotory = $this->getDoctrine()->getRepository(Seance::class);
        $data = new SearchData();
        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);
        $product = $repository->findSearch($data);
                //on va crer la methode findSearch dans productRepository qui va retourner un tableau de produit
        return $this->render('home/product.html.twig', [
            'products' => $product,
            'form' => $form->createView()
        ]);
    }

    
    /**
     * @Route("/cours", name="home_cours")
     */
    
    public function cours(){
        return $this->render('home/cours.html.twig');
    }
    
    /**
     * @Route("/create/new-Seance", name="new_seance")
     */
    public function creat(Request $request){
        return $this->render('home/create.html.twig');
    }


    /**
     * @Route("/delete/product/{id}", name="delete_product")
     */

    public function delete(Product $product){
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();

        $this->addFlash(
            'delete', 
            'Seance supprimer avec succès'
          );
        
        return $this->redirectToRoute('home_product');

        // return New Response('produit supprimé');

    }
  
    


}



